'''
Requests POST Multipart-Encoded File at a time
sending file as content 
'''

import asyncio
import aiohttp
import time

start_time = time.time()

url = "http://localhost:18081/fastDetect"
# files = {'file': open('APPL00714262_180_0.jpg','rb').read()}
files = {'file': open('APPL00668189_180_0.jpg','rb').read()} # BIG FILE
global success
success = 1
async def get_response(session,number):
    global success
    async with session.post(url, data=files) as response:
            rep = await response.text()
            if(rep!=""):
                success+=1
            
            print(number,rep)

async def main():
    async with aiohttp.ClientSession() as session:
        tasks = []
        for number in range(1, 100):
            tasks.append(asyncio.ensure_future(get_response(session,number)))
        
        hit_it = await asyncio.gather(*tasks)

text = asyncio.run(main())  # Assuming you're using python 3.7+
print("success",success)
print("--- %s seconds ---" % (time.time() - start_time))